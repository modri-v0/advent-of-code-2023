# frozen_string_literal: true
require 'trebuchet_calibrator'

RSpec.describe TrebuchetCalibrator do
  describe 'calibrate' do
    {'1': 11,
     '12': 12,
     '1abc2':12,
     'pqr3stu8vwx':38,
     'a1b2c3d4e5f':15,
     'treb7uchet': 77,
     'two1nine': 29,
     'eightwothree': 83,
     'abcone2threexyz': 13,
     'xtwone3four':24,
     'zyfive2seven':57,
     '7pqrstsixteen':76,
     '4nineeightseven2':42,
     'zoneight234':14,
     'lnzpqpsdvsix57oneightz':68}.each do |test_value, expectation|
      it "calibrates '#{test_value}' to #{expectation}" do
        sut = TrebuchetCalibrator.new

        expect(sut.calibrate(test_value.to_s)).to eq(expectation)
      end
    end
  end
end
