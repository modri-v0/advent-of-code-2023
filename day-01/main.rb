# frozen_string_literal: true

require 'trebuchet_calibrator'

# Codio para obtener la respuesta del ejercicio
# Podría reificarse
calibrator = TrebuchetCalibrator.new
total = 0
File.open('./resources/input.txt').readlines.each do |line|
  total += calibrator.calibrate(line)
end
puts total
