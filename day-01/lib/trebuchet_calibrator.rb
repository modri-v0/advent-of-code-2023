# frozen_string_literal: true
require 'strscan'

# Clase para manejar los digitos en formato string
class StringDigit
  def initialize(digit_string)
    @digit_string = digit_string
    @word_to_number = { "one":1,"two": 2, "three":3, "four":4, "five":5,"six":6,"seven":7, "eight":8,"nine":9}
  end
  def to_i
    Integer(@digit_string) rescue @word_to_number[@digit_string.to_sym]
  end
end

# Clase que maneja la calibración del trebuchet
class TrebuchetCalibrator

  def initialize
    @number_regexp = /1|2|3|4|5|6|7|8|9|one|two|three|four|five|six|seven|eight|nine/
  end
  def calibrate(an_string)
    only_string_digits = select_string_digits(an_string)
    as_two_digit_number(only_string_digits[0], only_string_digits[-1])
  end

  private

  def as_two_digit_number(first_digit, second_digit)
    (first_digit.to_i * 10) + second_digit.to_i
  end

  def select_string_digits(an_string)
    string_scanner= StringScanner.new(an_string)
    res = []
    (1..an_string.length).each do |i|
      res <<  StringDigit.new(string_scanner[0]) unless string_scanner.scan(@number_regexp).nil?
      string_scanner.pos = i
    end
    res
  end
end
