# Advent of Code 2023

Voy a utilizar el Advent of Code de 2023 para aprender algo más de Ruby.
La idea es resolver los problemas de cada utilizando TDD.

## Day 1

- [Puzzle](https://adventofcode.com/2023/day/1)
- [Code](./day-01)

# Day 2

- [Puzzle](https://adventofcode.com/2023/day/2)
- [Code](./day-02)

## Notas
No abstraer el conjunto de cubos de un color resultó en código repetido que se podría refactorizar.
