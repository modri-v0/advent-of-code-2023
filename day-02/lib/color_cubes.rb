# frozen_string_literal: true

# Representa un conjunto de cubos de un color
class ColorCubes
  COLOR_BLUE = "blue"
  COLOR_RED = "red"
  COLOR_GREEN = "green"

  attr_reader :color, :quantity
  def initialize(color:, quantity:)
    @color, @quantity = color, quantity
  end

  def self.empty(color:)
    new color:, quantity: 0
  end
  def self.blue(quantity)
    ColorCubes.new color: ColorCubes::COLOR_BLUE, quantity:
  end

  def self.red(quantity)
    ColorCubes.new color: ColorCubes::COLOR_RED, quantity:
  end

  def self.green(quantity)
    ColorCubes.new color: ColorCubes::COLOR_GREEN, quantity:
  end

  def ==(other)
    self.color == other.color && self.quantity == other.quantity
  end

  def is_smaller_or_equal_than(quantity)
    self.quantity <= quantity.quantity
  end

  def max(other)
    return other if self.quantity < other.quantity
    self
  end

  def add(other)
    sum = self.quantity + other.quantity
    ColorCubes.new color: self.color, quantity: sum
  end

end
