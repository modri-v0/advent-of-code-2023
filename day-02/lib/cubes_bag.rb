# frozen_string_literal: true
require 'color_cubes_set'
class CubesBag

  def initialize(color_cubes_set:)
    @color_cubes_set = color_cubes_set
  end

  def self.with(color_cubes)
    new color_cubes_set: (ColorCubesSet.of color_cubes:)
  end
  def self.containing(blue_cubes:0, red_cubes:0, green_cubes:0)
    color_cubes = [ColorCubes.blue(blue_cubes), ColorCubes.red(red_cubes), ColorCubes.green(green_cubes)]
    new color_cubes_set: (ColorCubesSet.of color_cubes:)
  end

  def self.empty
    containing
  end

  def can_generate?(color_cubes_set)
    color_cubes_set.has_less_cubes_than(@color_cubes_set)
  end

  def ==(other)
    @color_cubes_set == other.color_cubes_set
  end

  def adjust_for(a_color_cubes_set)
    CubesBag.new color_cubes_set: (a_color_cubes_set.maximize_cubes_with @color_cubes_set)
  end

  def power
    @color_cubes_set.inject(1) {|total, color_cube| total * color_cube.quantity}
  end

  protected
  attr_reader :color_cubes_set
end
