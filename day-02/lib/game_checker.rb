# frozen_string_literal: true

class GameChecker
  def is_a_valid_game?(game, bag)
    not(game.started?) || game.all_sets? {|set| bag.can_generate? set}
  end
end
