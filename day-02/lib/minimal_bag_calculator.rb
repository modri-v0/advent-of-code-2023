# frozen_string_literal: true

class MinimalBagCalculator
  def calculate_for(game)
    bag = CubesBag.empty
    game.all_sets_inject(bag) { | result_bag, set | result_bag.adjust_for(set)}
  end
end
