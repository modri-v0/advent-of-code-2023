# frozen_string_literal: true

require 'game_error'
require 'color_cubes_set'

class Game
  NOT_VALID_STRING_ERROR_MESSAGE = "Not a valid string to create a Game"
  attr_reader :id
  def initialize(id:1, sets:[])
    @id = id
    @sets = sets
  end

  def started?
    not(@sets.empty?)
  end

  def set_of(color_cubes:)
    @sets.push(ColorCubesSet.of color_cubes:)
  end
  def set(blue_cubes:0, red_cubes:0, green_cubes:0)
    self.set_of color_cubes: [ColorCubes.blue(blue_cubes), ColorCubes.red(red_cubes), ColorCubes.green(green_cubes)]
  end

  def all_sets_inject(initial_value, &block)
    @sets.inject(initial_value, &block)
  end

  def all_sets?(&condition)
    @sets.all? condition
  end

  def self.extract_sets(sets_str)
    return [] if sets_str.nil?
    sets_str.split(";").map{|set_str| ColorCubesSet.from_string(set_str)}
  end

  def self.from_string(str)
    game_str, sets_str = str.split ':'
    id = Game.extract_game_id(game_str)
    sets = Game.extract_sets(sets_str)
    Game.with id, sets
  end

  def self.extract_game_id(str)
    _, id = str.split " "
    raise GameError.new NOT_VALID_STRING_ERROR_MESSAGE if id.nil?
    raise GameError.new NOT_VALID_STRING_ERROR_MESSAGE unless id.match(/^\d+$/)
    id.to_i
  end
  def self.with(id, sets)
    new(id: id, sets: sets)
  end

  def sets_played
    @sets.length
  end
end
