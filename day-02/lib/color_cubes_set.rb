require 'game_error'
require 'color_cubes'
class ColorCubesSet

  NOT_VALID_STRING_ERROR_MESSAGE = "Not a valid string to create a GameSet"
  NOT_VALID_COLOR_ERROR_MESSAGE = "Not a valid color to create a GameSet"
  COLORS = %w[red blue green]
  def initialize(color_cubes:)
    @color_cubes = color_cubes
  end
  def self.of(color_cubes:)
    color_cubes.each { |color_cube| raise GameError, NOT_VALID_COLOR_ERROR_MESSAGE unless ColorCubesSet::COLORS.include?(color_cube.color)}
    ColorCubesSet.all_zero.add color_cubes
  end

  def self.all_zero
    color_cubes = ColorCubesSet::COLORS.each_with_object({}) do |color, empty_color_sets|
      empty_color_sets[color] = ColorCubes.empty color:
    end
    new color_cubes:
  end

  def self.from_string(str)
    color_cubes = str.split(",").map do |cube_str|
      quantity, color = cube_str.split " "
      raise GameError, NOT_VALID_STRING_ERROR_MESSAGE unless ColorCubesSet::COLORS.include?(color) && quantity.match(/^\d+$/)

      ColorCubes.new color:, quantity: quantity.to_i
    end
    of color_cubes:
  end

  def add(color_cubes)
    color_cubes_sum= color_cubes.each_with_object(@color_cubes.clone) do |color_cube, color_cubes_sum|
      color_cubes_sum[color_cube.color] = ColorCubes.empty color: color_cube.color  unless color_cubes_sum.has_key? color_cube.color
      color_cubes_sum[color_cube.color] = color_cube.add color_cubes_sum[color_cube.color]
    end
    ColorCubesSet.new color_cubes: color_cubes_sum
  end

  def ==(other)
    @color_cubes.keys.all? { |color|  @color_cubes[color] == other.color_cubes[color] }
  end
  def has_less_cubes_than(other)
    @color_cubes.keys.all? { |color|  @color_cubes[color].is_smaller_or_equal_than(other.color_cubes[color]) }
  end

  def maximize_cubes_with(other)
    color_cubes = @color_cubes.keys.map do |color|
      @color_cubes[color].max other.color_cubes[color]
    end
    ColorCubesSet.of color_cubes:
  end

  def inject(initial_value, &block)
    @color_cubes.values.inject(initial_value, &block)
  end

  protected
  attr_reader :color_cubes
end