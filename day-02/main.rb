# frozen_string_literal: true
require 'game_checker'
require 'game'
require 'cubes_bag'
require 'minimal_bag_calculator'

total = 0
power_total = 0
bag = CubesBag.containing red_cubes: 12, green_cubes: 13, blue_cubes: 14
game_checker = GameChecker.new
mbc = MinimalBagCalculator.new
File.open('./resources/input.txt').readlines.each do |line|
  game = Game.from_string(line)
  total += game.id if game_checker.is_a_valid_game?(game, bag)
  power_total += mbc.calculate_for(game).power
end
puts "part 1 total: #{total}"
puts "part 2 total: #{power_total}"