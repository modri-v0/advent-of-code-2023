# frozen_string_literal: true
require 'minimal_bag_calculator'
require 'game'
require 'cubes_bag'

RSpec.describe MinimalBagCalculator do

  describe 'calculate' do
    subject(:mbc) { MinimalBagCalculator.new }
    context 'when game have one set of 1 blue cube' do
      let(:game) {Game.from_string "Game 1: 1 blue"}
      let(:cubes_bag){CubesBag.with [ColorCubes.blue(1)]}
      it "calculate a cubes bag of 1 blue cube" do
        expect(mbc.calculate_for game).to eq(cubes_bag)
      end
    end

    context 'when game have one set of 1 red cube' do
      let(:game) {Game.from_string "Game 1: 1 red"}
      let(:cubes_bag){CubesBag.with [ColorCubes.red(1)]}
      it "calculate a cubes bag of 1 red cube" do
        expect(mbc.calculate_for game).to eq(cubes_bag)
      end
    end

    context 'when game have one set of 1 green cube' do
      let(:game) {Game.from_string "Game 1: 1 green"}
      let(:cubes_bag){CubesBag.with [ColorCubes.green(1)]}
      it "calculate a cubes bag of 1 green cube" do
        expect(mbc.calculate_for game).to eq(cubes_bag)
      end
    end

    context 'when game have three sets of 1 different color cube' do
      let(:game) {Game.from_string "Game 1: 1 green; 1 blue; 1 red"}
      let(:cubes_bag){CubesBag.with [ColorCubes.blue(1), ColorCubes.red(1),ColorCubes.green(1)]}

      it "calculate a cubes bag of 1 green, 1 blue and 1 red cube" do
        expect(mbc.calculate_for game).to eq(cubes_bag)
      end
    end
  end
end