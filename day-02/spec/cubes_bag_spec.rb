# frozen_string_literal: true
require 'cubes_bag'

RSpec.describe CubesBag do

  describe 'power' do
    context 'when cubes quantities by color is 1' do
      subject(:cubes_bag) { CubesBag.with [ColorCubes.blue(1), ColorCubes.red(1), ColorCubes.green(1)]}

      it 'should be one' do
        expect(cubes_bag.power).to eq(1)
      end
    end
    context 'when cubes quantities are 2 blue, 3 red and 5 green' do
      subject(:cubes_bag) { CubesBag.with [ColorCubes.blue(2), ColorCubes.red(3), ColorCubes.green(5)]}

      it 'should be 30' do
        expect(cubes_bag.power).to eq(30)
      end
    end
  end
end
