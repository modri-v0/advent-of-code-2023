# frozen_string_literal: true
require 'game'
require 'cubes_bag'
require 'game_checker'

RSpec.describe GameChecker do
  subject(:game_checker) { GameChecker.new }
  let(:game) {Game.new}
  describe "isAValidGame?" do
    context "an empty cubes bag and a new game" do
      let(:bag) {CubesBag.empty}
      it 'checks as a valid game' do
        expect(game_checker.is_a_valid_game?(game, bag)).to eq(true)
      end
    end

    context "a cubes bag with 1 blue cube and a new game" do
      let(:bag) {CubesBag.with [ColorCubes.blue(1)]}
      it 'checks as a valid game' do
        expect(game_checker.is_a_valid_game?(game, bag)).to eq(true)
      end
    end

    context "a cubes bag with 1 red cube and a new game" do
      let(:bag) {CubesBag.with [ColorCubes.red(1)]}
      it 'checks as a valid game' do
        expect(game_checker.is_a_valid_game?(game, bag)).to eq(true)
      end
    end

    context "a cubes bag with 1 green cube and a new game" do
      let(:bag) {CubesBag.with [ColorCubes.green(1)]}
      it 'checks as a valid game' do
        expect(game_checker.is_a_valid_game?(game, bag)).to eq(true)
      end
    end

    context "an empty cubes bag and a game with a set with 1 blue cube" do
      let(:bag) {CubesBag.empty}
      let(:color_cubes) {[ColorCubes.blue(1)]}
      it 'checks as an invalid game' do
        game.set_of color_cubes: color_cubes
        expect(game_checker.is_a_valid_game?(game, bag)).to eq(false)
      end
    end

    context "an empty cubes bag and a game with a set with 1 red cube" do
      let(:bag) {CubesBag.empty}
      let(:color_cubes) {[ColorCubes.red(1)]}
      it  'checks as an invalid game' do
        game.set_of color_cubes: color_cubes
        expect(game_checker.is_a_valid_game?(game, bag)).to eq(false)
      end
    end

    context "an empty cubes bag and a game with a set with 1 green cube" do
      let(:bag) {CubesBag.empty}
      let(:color_cubes) {[ColorCubes.green(1)]}
      it  'checks as an invalid game' do
        game.set_of color_cubes: color_cubes
        expect(game_checker.is_a_valid_game?(game, bag)).to eq(false)
      end
    end

    context "an empty cubes bag and a game with a set with 1 green cube" do
      let(:color_cubes_set_1) {[ColorCubes.blue(1)]}
      let(:color_cubes_set_2) {[ColorCubes.red(1)]}
      let(:bag) {CubesBag.with color_cubes_set_1}
      it  'checks as an invalid game' do
        game.set_of color_cubes: color_cubes_set_1
        game.set_of color_cubes: color_cubes_set_2
        expect(game_checker.is_a_valid_game?(game, bag)).to eq(false)
      end
    end

    context "an empty cubes bag and a game with a set with 1 green cube" do
      let(:color_cubes_set_1) {[ColorCubes.blue(1)]}
      let(:color_cubes_set_2) {[ColorCubes.red(1)]}
      let(:bag) {CubesBag.with [ColorCubes.blue(1), ColorCubes.red(1)]}
      it  'checks as an invalid game' do
        game.set_of color_cubes: color_cubes_set_1
        game.set_of color_cubes: color_cubes_set_2
        expect(game_checker.is_a_valid_game?(game, bag)).to eq(true)
      end
    end
  end
end