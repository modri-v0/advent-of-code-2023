# Example 'Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green'
require 'game'
require 'cubes_bag'

RSpec.describe Game do

  describe '.fromString' do
    context 'when string is "Game x"' do
      context "with x the number 2" do
        subject(:game_from_string) { described_class.from_string "Game 2" }
        it "id should be 2" do
          expect(game_from_string.id).to eq(2)
        end
        it { expect(game_from_string).not_to be_started }
      end
      context "with x not a number" do
        it {expect { Game.from_string "Game x" }.to raise_error(GameError, Game::NOT_VALID_STRING_ERROR_MESSAGE)}
      end
    end

  context 'when string is "Game 1: sets_str"' do
    context 'with sets_str == "3 blue" ("number color")' do
      subject(:game_from_string) { described_class.from_string "Game 1: 3 blue" }
      let(:bag){ CubesBag.containing blue_cubes: 3}
      it { expect(game_from_string).to be_started }
      it "set played should be 1" do
        expect(game_from_string.sets_played).to eq(1)
      end
      it "set can be generated with bag containing 3 blue cubes" do
        expect(game_from_string.all_sets? {|set| bag.can_generate? set})
      end
    end
    context 'with sets_str == "3 blue, 2 green" ("number color_1, number color_2")' do
      subject(:game_from_string) { described_class.from_string "Game 1: 3 blue" }
      let(:bag){ CubesBag.containing blue_cubes: 3, green_cubes: 2}
      it { expect(game_from_string).to be_started }
      it "set played should be 1" do
        expect(game_from_string.sets_played).to eq(1)
      end
      it "set can be generated with bag containing 3 blue cubes, 2 green cubes" do
        expect(game_from_string.all_sets? {|set| bag.can_generate? set})
      end
    end
    context 'with sets_str == "3 blue; 2 red" ("number color_1; number color_2")' do
      subject(:game_from_string) { described_class.from_string "Game 1: 3 blue; 2 red" }
      let(:bag){ CubesBag.containing blue_cubes: 3, red_cubes: 2}
      it { expect(game_from_string).to be_started }
      it "sets played should be 2" do
        expect(game_from_string.sets_played).to eq(2)
      end
      it "sets can be generated with bag containing 3 blue cubes, 2 green cubes" do
        expect(game_from_string.all_sets? {|set| bag.can_generate? set})
      end
    end
    context 'with sets_str == "x blue" ("not_number color")' do
      it {expect { Game.from_string "Game 1: x blue" }.to raise_error(GameError, ColorCubesSet::NOT_VALID_STRING_ERROR_MESSAGE)}
    end
    context 'with sets_str == "2 brown" ("number non_valid_color")' do
      it {expect { Game.from_string "Game 1: 2 brown" }.to raise_error(GameError, ColorCubesSet::NOT_VALID_STRING_ERROR_MESSAGE)}
    end
  end
  end

end